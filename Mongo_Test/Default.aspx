﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="Mongo_Test._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        #Submit1
        {
            height: 26px;
            width: 113px;
        }
        #Text1
        {
            width: 224px;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Welcome to MongoDB
      
    </h2>
     <a href="NewCustomer.aspx">[Create New Customer]</a>
    <p>
 
        &nbsp;Email:  &nbsp;<asp:TextBox ID="textSearch" runat="server"></asp:TextBox>
        <asp:Button ID="btnSearch" runat="server" Text="Search" 
            onclick="btnSearch_Click" />
      </p>
    <%if (customers != null)
        {  %>
        <table style="width: 912px">
            <tr>
                <td bgcolor="#99CCFF" style="border-width: medium; border-style: none;">Id</td>
                <td bgcolor="#99CCFF" style="border-width: medium; border-style: none;">Name</td>
                <td bgcolor="#99CCFF" style="border-width: medium; border-style: none;">Email</td>
                <td bgcolor="#99CCFF" style="border-width: medium; border-style: none;"></td>
            </tr>
            <%foreach (var doc in customers)
               { %>
               <tr>
                 <td> <%=doc.Id %></td>
                <td> <%=doc.Name %></td>
                <td> <%=doc.Email %></td>  
                <td><a href="EditCustomer.aspx?id=<%=doc.Id %>">Edit</a></td>
             </tr>
            <%}%>
        </table>
    <%} %>
    </asp:Content>
