﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using Mongo_Test.AppCode;

namespace Mongo_Test
{

    public partial class _Default : System.Web.UI.Page
    {
        public MongoCursor<Customer> customers;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string id = Request.QueryString["id"];
                if (!string.IsNullOrEmpty(id))
                {
                    MongoDatabase database  = ConnectDatabase.GetDatabase("Customer");
                    string keyword = textSearch.Text;
                    var query = Query.EQ("_id", ObjectId.Parse(id));
                    customers = database.GetCollection<Customer>("CustomerData").Find(query);
                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            MongoDatabase database  = ConnectDatabase.GetDatabase("Customer");
            string keyword = textSearch.Text;
            string query_search = string.Format("/{0}/", keyword);

            var query = Query.Matches("email", new BsonRegularExpression(query_search));
            customers = database.GetCollection<Customer>("CustomerData").Find(query);

        }
        protected void Create_Click(object sender, EventArgs e)
        {
            Response.Redirect("NewCust.aspx");
        }
       
    

    }
}
