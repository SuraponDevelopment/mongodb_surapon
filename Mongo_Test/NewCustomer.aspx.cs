﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MongoDB.Driver;
using MongoDB.Bson;
using Mongo_Test.AppCode;

namespace Mongo_Test
{
    public partial class NewCustomer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Create_Click(object sender, EventArgs e)
        {
            MongoDatabase database = ConnectDatabase.GetDatabase("Customer");
            MongoCollection<BsonDocument> customer = database.GetCollection("CustomerData");
            BsonDocument employee = new BsonDocument {
                        { "name", name.Text },
                        { "email", email.Text },
                        { "level", level.Text },
                        { "createddate", DateTime.Now }
                        };
            var result = customer.Insert(employee);

            if (result.Ok)
            {
                Response.Redirect("default.aspx");
            
            }
        }
    }
}