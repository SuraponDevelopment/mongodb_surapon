﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditCustomer.aspx.cs" Inherits="Mongo_Test.EditCustomer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
      <%if (customers != null)
        {  %>

               <fieldset><legend>Edit Customer</legend>
              <table>
                      <tr>
                         <td>Id</td>
                         <td> <asp:Label ID="IdCust" runat="server" Text="Label"></asp:Label> </td>
                      </tr> 
                       <tr>
                         <td>Name</td>
                         <td><asp:TextBox ID="name" runat="server"></asp:TextBox></td>       
                      </tr>
                       <tr>
                      <td>Email</td>
                         <td><asp:TextBox ID="email" runat="server"></asp:TextBox></td>                   
                      </tr>
                       <tr>
                      <td>Level</td>
                         <td><asp:TextBox ID="level" runat="server"></asp:TextBox></td>                   
                      </tr>
                      <tr>
                       <td></td>
                      <td><asp:Button ID="Update" runat="server" Text="Update" onclick="Update_Click" /> <asp:Button ID="Delete" runat="server" Text="Delete" onclick="Delete_Click" /></td>
                      </tr>
              </table>
                     
               </fieldset>
          
      <%} %>
      <%else{ %>
            
    <a href="Default.aspx">Back</a>
       <%}%>
       </div>
</asp:Content>
