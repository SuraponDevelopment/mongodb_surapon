﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MongoDB.Driver;
using Mongo_Test.AppCode;
using MongoDB.Driver.Builders;
using MongoDB.Bson;

namespace Mongo_Test
{
    public partial class EditCustomer : System.Web.UI.Page
    {  public Customer customers;
        protected void Page_Load(object sender, EventArgs e)
        {
          
            if (!Page.IsPostBack)
            {
                string id = Request.QueryString["id"];
                MongoDatabase database = ConnectDatabase.GetDatabase("Customer");

                if (!string.IsNullOrEmpty(id))
                {

                    var query = Query.EQ("_id", ObjectId.Parse(id));
                    customers = database.GetCollection<Customer>("CustomerData").FindOne(query);

                    IdCust.Text = customers.Id.ToString();
                    name.Text = customers.Name;
                    email.Text = customers.Email;
                    level.Text = customers.Level;
                }

            }
        }
        protected void Update_Click(object sender, EventArgs e)
        {
            string id = Request.QueryString["id"];
            MongoDatabase database = ConnectDatabase.GetDatabase("Customer");

            var query = Query.EQ("_id", ObjectId.Parse(IdCust.Text));


            var update = Update<Customer>.
                            Set(p => p.Email, email.Text).
                            Set(p => p.Level,level.Text).
                            Set(p => p.Name, name.Text);

            var result = database.GetCollection<Customer>("CustomerData").Update(query, update);
            if (result.Ok)
            {
                Response.Redirect("default.aspx?id=" + IdCust.Text);

            }
        }
        protected void Delete_Click(object sender, EventArgs e)
        {
            string id = Request.QueryString["id"];
            MongoDatabase database = ConnectDatabase.GetDatabase("Customer");
            var query = Query.EQ("_id", ObjectId.Parse(IdCust.Text));
            var result = database.GetCollection<Customer>("CustomerData").Remove(query);
            if (result.Ok)
            {
                Response.Redirect("default.aspx?id" + IdCust.Text);

            }
        }
    }
}