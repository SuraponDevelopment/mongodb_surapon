﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NewCustomer.aspx.cs" Inherits="Mongo_Test.NewCustomer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <fieldset><legend>New Customer</legend>
              <table>
                       <tr>
                         <td>Name</td>
                         <td><asp:TextBox ID="name" runat="server"></asp:TextBox></td>       
                      </tr>
                      <tr>
                      <td>Email</td>
                         <td><asp:TextBox ID="email" runat="server"></asp:TextBox></td>       
                               
                      </tr>
                       <tr>
                      <td>Level</td>
                         <td><asp:TextBox ID="level" runat="server"></asp:TextBox></td>                   
                      </tr>
                      <tr>
                       <td></td>
                      <td><asp:Button ID="Create" runat="server" Text="Create" onclick="Create_Click" /></td>
                      </tr>
              </table>
               
               
               </fieldset>
    </div>
        <a href="Default.aspx">Back</a>
</asp:Content>
