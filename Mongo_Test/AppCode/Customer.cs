﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace Mongo_Test.AppCode
{
    public class Customer
    {
        [BsonElementAttribute("_id")]
        public ObjectId Id { set; get; }
        [BsonElementAttribute("name")]
        public string Name { set; get; }
        [BsonElementAttribute("email")]
        public string Email { set; get; }
        [BsonElementAttribute("createddate")]
        public DateTime CreateDate { set; get; }
        [BsonElementAttribute("level")]
        public string Level { set; get; }
    }
}