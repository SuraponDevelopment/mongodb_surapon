﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Driver;

namespace Mongo_Test.AppCode
{
    public class ConnectDatabase
    {
        public static MongoDatabase GetDatabase(string databasename)
        {
            MongoClient client = new MongoClient();
            MongoServer server = client.GetServer();
            server.Connect();
            MongoDatabase database = server.GetDatabase(databasename);

            return database;

        }
    }
}